#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/ip.h>

/* 
  This firewall supports varying enforcement modes as well as ufw-like rules 
  (filter incoming, outgoing by ip/port/interface/protocol).
  
  Rules are defined in a list below (see struct frule) and are evaluated 
  according to the enforcement mode.

  Built on LKM & Netfilter
*/

/* Rule enforcement modes:
  - STRICT: If any applicable rule denies, it is denied.
  - LENIENT: If any applicable rule allows, it is allowed.
  - LAST_RULE: Final verdict is the verdict of the last applicable rule added.
*/
typedef enum { STRICT, LENIENT, LAST_RULE } Enforce;

/* Firewall Rule struct
  - int allow: return code for hook_func (NF_ALLOW=1 or NF_DENY=0)
  - unsigned char * sip: source IP address string
  - int sport: source port number
  - unsigned char * dip: destination IP address string
  - int dport: destination port number
  - char * interface: device name string (e.g. eth0)
  - __u8 protocol: network protocol packet follows
*/
typedef struct frule {
  int allow;
  unsigned char * sip;
  int sport;
  unsigned char * dip;
  int dport;
  char * interface;
  __u8 protocol;
  int inbound;
} frule;

static struct nf_hook_ops nf_ops_incoming; // Netfilter incoming options struct
static struct nf_hook_ops nf_ops_outgoing; // Netfilter outgoing options struct
static const Enforce enforce_mode = STRICT; // Verdict is default or first to deny
static const int DEFAULT_VERDICT = NF_ACCEPT; // Default verdict if none apply
static frule rules[] = { // Hard coded table of rules
  // ===========================
  // port -1 means ALL
  // port 0 means EXEMPT
  // empty address means EXEMPT
  // * address means ALL
  // * interface means all
  // ===========================
  // deny B packets from any port to my port 22 on eth0, tcp
  {0, "10.0.2.4", -1, "10.0.2.5", 22, "eth0", IPPROTO_TCP, 1},
  // deny B packets from any port to my port 22 on eth0, udp
  {0, "10.0.2.4", -1, "10.0.2.5", 22, "eth0", IPPROTO_UDP, 1},
  // deny B packets from any port to my port 23 on eth0, tcp
  {0, "10.0.2.4", -1, "10.0.2.5", 23, "eth0", IPPROTO_TCP, 1},
  // allow B packets from any port to my port 23 on eth0, tcp
  {1, "10.0.2.4", -1, "10.0.2.5", 23, "eth0", IPPROTO_TCP, 1},
  // allow B packets from any port to my port 23 on eth0, tcp
  {1, "10.0.2.4", -1, "10.0.2.5", 80, "eth0", IPPROTO_TCP, 1},
  // deny B packets from any port to my port 23 on eth0, tcp
  {0, "10.0.2.4", -1, "10.0.2.5", 80, "eth0", IPPROTO_TCP, 1},
  // deny my ability to contact google
  {0, "10.0.2.5", -1, "216.58.192.142", -1, "*", IPPROTO_TCP, 0},
  {0, "10.0.2.5", -1, "216.58.192.142", -1, "*", IPPROTO_UDP, 0},
  // deny google packets from any port to any of my ports on eth0, tcp (blocks webpage on STRICT)
  {0, "172.217.4.228", -1, "10.0.2.5", -1, "*", IPPROTO_TCP, 1},
  // excuse one port on the google ban (webpage accessible on LAST_RULE mode)
  {1, "216.58.192.142", 443, "10.0.2.5", -1, "*", IPPROTO_TCP, 1},
  {1, "172.217.4.228", -1, "10.0.2.5", -1, "*", IPPROTO_TCP, 1},
  {1, "10.0.2.5", -1, "216.58.192.142", 443, "*", IPPROTO_TCP, 0},
  {1, "10.0.2.5", -1, "172.217.4.228", -1, "*", IPPROTO_TCP, 0},
  // Block anyone from seeing my apache server
  {0, "*", -1, "10.0.2.5", 80, "*", IPPROTO_TCP, 1},
  {0, "*", -1, "10.0.2.5", 80, "*", IPPROTO_UDP, 1}
};

// Helper for ALL notation
int is_all_str(char * str) {
  return !strcmp(str, "*");
}

// Helper for EXEMPT notation
int is_exempt_str(char * str) {
  return !strcmp(str, "");
}

// Helper for ALL notation
int is_all_int(int i) {
  return i == -1;
}

// Helper for EXEMPT notation
int is_exempt_int(int i) {
  return i == 0;
}

// Helper to check if interface applies to rule
int interface_match(char * rinterface, char * interface) {
  return is_all_str(rinterface) || !strcmp(interface, rinterface);
}

// Helper to check if port applies to rule
int port_match(int rport, int port) {
  return is_all_int(rport) || 
        (!is_exempt_int(rport) && rport == port);
}

// Helper to check if ip address applies to rule
int ipaddr_match(char * raddr, char * addr) {
  return is_all_str(raddr) || 
          (!is_exempt_str(raddr) && !strcmp(raddr, addr));
}

void print_verdict(int verdict) {
  printk("Verdict: %s\n\n", verdict ? "ALLOW" : "DENY");
}

// Helper to check if incoming/outgoing matches rule
int direction_match(int rinbound, int inbound) {
  return rinbound == -1 || rinbound == inbound;
}

// Netfilter hook
static unsigned int hook_func(const struct nf_hook_ops *ops,
                              struct sk_buff *skb,
                              const struct net_device *in,
                              const struct net_device *out,
                              int (*okfn)(struct sk_buff *),
                              int incoming) {

  struct iphdr *iph;            /* IPv4 header */
  struct tcphdr *tcph;          /* TCP header */
  struct udphdr *udph;          /* UDP header */
  char * interface;             /* packet device interface */
  int sport = 0, dport = 0;     /* Source and destination ports */
  char saddr[16], daddr[16];    /* Source and destination addresses */
  int protocol = 0;             /* Packet protocol */
  //unsigned char *user_data;   /* TCP/UDP data begin pointer */
  //unsigned char *tail;        /* TCP/UDP data end pointer */
  //unsigned char *it;          /* TCP/UDP data iterator */

  if (!skb) // Packet was empty, whatever
    return DEFAULT_VERDICT;
  iph = ip_hdr(skb);
  interface = in->name;
  if (!strcmp(interface, "")) // default interface cause net_device sucks
    interface = "eth0";
  protocol = iph->protocol;
  /* Gather relevant IP address strings */
  snprintf(saddr, 16, "%pI4", &iph->saddr);
  snprintf(daddr, 16, "%pI4", &iph->daddr);
  printk("Device in: \"%s\"\n", interface);

  switch (protocol) {
    case IPPROTO_UDP:
      printk("Protocol: UDP\n");
      udph = udp_hdr(skb);
      sport = ntohs(udph->source);
      dport = ntohs(udph->dest);
    break;
    case IPPROTO_TCP:
      printk("Protocol: TCP\n");
      tcph = tcp_hdr(skb);
      sport = ntohs(tcph->source);
      dport = ntohs(tcph->dest);
    break;
    default:
      printk("Protocol: %d (unrecognized)\n", protocol);
    break;
  }
  printk("Source port: %d, addr: %s\n", sport, saddr);
  printk("Dest port: %d, addr: %s\n", dport, daddr);
  int verdict = DEFAULT_VERDICT;
  int i;
  // Loop over rules. If rule applies, apply verdict
  for (i = 0; i < sizeof(rules) / sizeof(frule); i++) {
    frule rule = rules[i];
    // Evaluate if rule applies to packet:
    // No variables = short circuit
    if (direction_match(rule.inbound, incoming) &&
        rule.protocol == protocol && 
        interface_match(rule.interface, interface) && 
        port_match(rule.sport, sport) && 
        port_match(rule.dport, dport) && 
        ipaddr_match(rule.sip,  saddr) && 
        ipaddr_match(rule.dip,  daddr)) {
      verdict = rule.allow;
      // If mode is STRICT and verdict is not allow, return
      // If mode is LENIENT and verdict is allow, return
      if ((enforce_mode == STRICT && verdict != NF_ACCEPT) ||
          (enforce_mode == LENIENT && verdict == NF_ACCEPT)) {
        print_verdict(verdict);
        return verdict;
      }
    }
  }
  print_verdict(verdict);
  return verdict;
}

// Wrapper to determine direction
static unsigned int hook_func_out(const struct nf_hook_ops *ops,
                              struct sk_buff *skb,
                              const struct net_device *in,
                              const struct net_device *out,
                              int (*okfn)(struct sk_buff *)) {
  return hook_func(ops, skb, in, out, okfn, 0);
}

// Wrapper to determine direction
static unsigned int hook_func_in(const struct nf_hook_ops *ops,
                              struct sk_buff *skb,
                              const struct net_device *in,
                              const struct net_device *out,
                              int (*okfn)(struct sk_buff *)) {
  return hook_func(ops, skb, in, out, okfn, 1);
}

// Kernel module initialization
// EZ run: make clean && make && sudo insmod mfirewall.ko
int init_module() {
  nf_ops_incoming.hook = (nf_hookfn *) hook_func_in;
  nf_ops_incoming.pf = PF_INET;
  // handle incoming requests
  nf_ops_incoming.hooknum = NF_INET_PRE_ROUTING;
  nf_ops_incoming.priority = NF_IP_PRI_FIRST;
  int res = nf_register_hook(&nf_ops_incoming);
  if (res < 0) {
    pr_err("mfirewall: error in nf_register_hook()\n");
  }

  nf_ops_outgoing.hook = (nf_hookfn *) hook_func_out;
  nf_ops_outgoing.pf = PF_INET;
  // handle outgoing requests
  nf_ops_outgoing.hooknum = NF_INET_POST_ROUTING;
  nf_ops_outgoing.priority = NF_IP_PRI_FIRST;
  res = nf_register_hook(&nf_ops_outgoing);
  if (res < 0) {
    pr_err("mfirewall: error in nf_register_hook()\n");
  }
  pr_debug("mfirewall: loaded\n");
  return 0;
}

// Kernel module cleanup
void cleanup_module() {
  nf_unregister_hook(&nf_ops_incoming);
  nf_unregister_hook(&nf_ops_outgoing);
  pr_debug("mfirewall: unloaded\n");
}