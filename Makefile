ifeq ($(KERNELRELEASE),)

KERNELDIR ?= /lib/modules/$(shell uname -r)/build

module:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

clean:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) clean

.PHONY: module clean

else

MODULE = mfirewall.o
CFLAGS_$(MODULE) := -DDEBUG
obj-m := $(MODULE)

endif